Here are some ideas for learning adventures! Feel free to add to this list :) 

- Pick a book you've been meaning to read, define some questions you have before
  hand, and then after reading it write how the book did or did not answer your questions.
  
- Get together a group and find an expert in your field to give you a short
  series of lectures (20-30min) in a casual setting.
