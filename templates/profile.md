# Background
<!-- What's your background? What are the skills/ideas/fields that you work with
or are familiar with? -->

# Interests

<!-- What are the things you're interested in learning in FNS? -->

# Learning Victories!

<!-- This section is for you to brag about the things you've learned! Researched
something cutting edge? Made the first baby-steps into a new field? Celebrate it
here-->

# Your spaces
<!-- You'll need at least a website and a gitlab account to get the most out of -->
<!-- FNS. If you need any help getting set up, just ask here!-->

- **website**:
- **gitlab**:
- **social _(optional)_**:
