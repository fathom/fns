Hi! Welcome to FNS, we're glad to have you :) 

This is a community for **self-directed learning** about **programming and
systems**.

It's a place to develop the ways in which you learn, embark on learning
adventures, and discuss and collaborate with others.

Unlike normal learning institutions there are no structures imposed on you.
Instead there's infrastructure and support for you to develop the structures
that make sense for _you_.

This manual is intended to get you familiar with that infrastructure and how to
make the most of it.

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [Tools and Resources](#tools-and-resources)
- [Getting Started](#getting-started)
    - [What You'll Need](#what-youll-need)
    - [Introducing yourself](#introducing-yourself)
    - [Exploring the community](#exploring-the-community)
- [Starting Your Adventure](#starting-your-adventure)
    - [What is a learning adventure?](#what-is-a-learning-adventure)
        - [Suggestions](#suggestions)
    - [Embarking on your learning adventure](#embarking-on-your-learning-adventure)
    - [Retrospecting](#retrospecting)
        - [Possible questions:](#possible-questions)
- [Your first adventure](#your-first-adventure)

<!-- markdown-toc end -->

# Tools and Resources

We use [Discourse](https://www.discourse.org/), an open source discussion
platform. This manual contains links to different pages on Discourse, which you
won't be able to access if you aren't logged in.

# Getting Started

## What You'll Need

You need a couple things to get the most out of FNS, a **website** and a
**gitlab account**. We use git to collaborate on learning adventures and on
structures for FNS (like this guidebook your reading). Our personal websites act
as the homebase for our learning, letting us document things and interact online
from a place firmly within our control.

If you don't have these things, or don't know where to start, **don't
worry**, we're here to support you learning about them.

## Introducing yourself
The first step to participating in FNS is introducing yourself. Go on over to
the [Profiles](https://fns.fathom.network/c/profiles) category in Discourse and
create a new topic. The new topic template should guide you through what
information is good to include. Feel free to include as much or as little as
you're comfortable with. 

If you don't yet have all of the [things you'll need](#what-youll-need) this is
where you can say so, and so we can help get you set up.

Notice that this category is called Profiles, not Introductions. That's because
this post isn't static, you can come back and edit it as your learning changes.

## Exploring the community

Once you've introduced yourself, take some time to get familiar with what's
going on in FNS. Take a look at the [learning
adventures](https://fns.fathom.network/c/adventures) people are embarking on,
hop in [discussions](https://fns.fathom.network/c/discussion), or get
[meta](https://fns.fathom.network/c/meta) and join the conversation on how to
improve FNS. 

Once you're ready it's time to set out on your own learning adventure.

# Starting Your Adventure

## What is a learning adventure?

Adventures are the heart of FNS. Put simply, they're explicitly defined learning
experiences. You write out a post in the
[adventures](https://fns.fathom.network/c/adventures) category defining:
 - a subject: what you want to learn
 - a scope: when you're going to start and finish
 - a structure: *how* you're going to learn it
 
You can embark on an adventure on your own, or coordinate with others. It could
take the form of a independant study, or producing a paper, or attending
lectures, anything you can think of!

Setting out on a learning adventure can be intimidating. Most of us haven't had
too much experience in creating our own systems for learning, it's not really
what current educational systems are designed for. The key is to remember you're
learning to learn. It's a muscle that takes excersise to grow. 

### Suggestions

All of these things are ultimately up to you, but we do have some suggestions:

1. If you don't already have scope in mind, start with a weeklong learning
   adventure. 

   It's enough time to try something out, and get into the meat of a subject, even
   with other commitments and priorities.

2. For a subject, try picking something different from your "day job"
   
   Exploring a new or adjacent subject can push you to look more carefully at
   _how_ you're learning. You also won't have the strucutre of expertise, which
   will push you to create your own. It's also just exciting to try new things
   out, and excitment is worth cultivating.
   
3. be as intentional and specific but don't be afraid to make changes and shift
   course. 

   It's better to try something specific, **learn** why it doesn't work, and
   then change, rather than to do something open ended enough to encompass
   anything.

4. Questions are useful tools to guide you, as are explict timelines, conversations
   with others, and producing artificats. 

5. If you're worried about keeping momentum, put an accountability schedule in
   your structure, i.e post an update every x days or something similar!

6. Try things out, and ask for help!

## Embarking on your learning adventure

When you're ready, create a new post in the
[adventures](https://fns.fathom.network/c/adventures) category describing your
adventure. Don't worry about getting the language right or tweaking it, just get
something out there.

This post is for you to track your adventure and for the communitty support you.
That could be in the form of suggestions, answering questions, teaming up and
joining your adventure, or just cheering you on. 

If things change, or you deviate from your plan **don't worry!** Just come back
and update your post to reflect what you decided to do.

## Retrospecting

The learning doesn't stop when you're adventure is done! When you've hit the
time you set out in your plan, come back to the post you made and post a little
retrospective. It doesn't need to be long, just something to review how your
adventure went down.

### Possible questions:

- Were you're learning structures effective?
- What was the delta between your expectations of the subject and reality?


# Your first adventure

There are a couple things that are really useful for you as a learner. One of
those is an online identity, a website and a domain name you control. If you
don't already have it, this is a great way to anchor your first learning adventure.

The structure for your learning adventure can be really simple: 

1. Set yourself a question to answer. It can be anything, as long as you think
   you could reasonably answer it in one week.
   
2. Get a domain name and figure out how to publish a website to it. Services
   like [netlify](https://netlify.com) or [wordpress](https://wordpress.com) are
   perfect for this, but if you get stuck just ask for help from the
   [community](https://fns.fathom.network)
3. Publish a post on your new website answering these questions: 
   1. What was the answer to your question (or as far as you got)?
   2. What tools or systems did you use to find that answer?
   3. How was the learning you had to do to create your website connected to the
      learning you had to do to answer your question?
      
The idea here is to do two things at once, both learn a particular subject, and
set up infrastructure for your learning. These are both things you'll constantly
have to do as you grow as a learner, so we want to start practicing asap!
